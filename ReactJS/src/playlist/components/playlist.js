import React from 'React';
import Media from "./media";
import "./playlist.css";

function Playlist(props) {
  return (
    <div className='Playlist'>
      {
        props.playlist.map((data) => {
          return <MediaConatiner 
                    {...data}
                    key={data.id}
                    openModal={props.handleOpenModal} 
                  />
        })
      }
    </div>
  )
}

export default Playlist;