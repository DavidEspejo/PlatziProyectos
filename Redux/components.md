Platzi-Video <3
================
home - entry point para webpack

  Home - página / (container|smart)
    -> <!-- Layout - UI -->
      -> Related - UI
      -> Categories - UI
        -> Category - UI
           -> Playlist - UI
            -> Media - UI / Pure
    -> Search / (container|smart)
    -> Modal - (container|smart)
      -> <!-- Layout - UI -->
        -> VideoPlayer - (container|smart)
          <!-- Layout -->
          -> Video - UI state
          -> Spinner - UI
          -> Controls - UI
            -> PlayPause - UI
              -> PlayIcon - UI
              -> PauseIcon - UI
            -> Timer - UI
            -> ProgressBar - UI
            -> Volume - UI
              -> VolumeIcon - UI
            -> FullScreen - UI
              -> FullScreenIcon - UI